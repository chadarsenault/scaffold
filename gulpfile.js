// Include gulp
var gulp = require('gulp');

// Include plugins
var lint = require('gulp-jshint');
var compass = require('gulp-compass');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var minify_css = require('gulp-minify-css');
var watch = require('gulp-watch');
var rename = require('gulp-rename');
var reload = require('gulp-livereload');
var beep = require('beepbeep');
var colors = require('colors');
var plumber = require('gulp-plumber');

// Lint task
gulp.task('jshint', function() {
	gulp.src('src/assets/js/*.js')
	.pipe(lint())
	.pipe(lint.reporter('default'));
});

// SASS task
gulp.task('sass', function() {
	gulp.src('src/assets/scss/*.scss')
	.pipe(
		plumber(function(err) {
			beep();
			console.log('[sass] '.magenta + 'There was an issue compiling Sass\n' + err.yellow);
			this.emit('end');
		})
	)
	.pipe(compass({
		css: 'dist/assets/css/expanded',
		sass: 'src/assets/scss',
		require: [
			'susy',
			'breakpoint'
		],
		style: 'expanded',
		environment: 'development'
	}))
	.pipe(rename('global.min.css'))
	.pipe(minify_css())
	.pipe(gulp.dest('dist/assets/css'));
});

// Vendor CSS task
gulp.task('css', function() {
	gulp.src('src/assets/css/*.css')
	.pipe(
		plumber(function(err) {
			beep();
			console.log('[css] '.magenta + 'There was an issue minifying CSS\n' + err.yellow);
			this.emit('end');
		})
	)
	.pipe(rename('vendor.min.css'))
	.pipe(minify_css())
	.pipe(gulp.dest('dist/assets/css'));
});

// Font icon task
gulp.task('font_copy', function() {
	gulp.src('src/assets/fonts/*.*')
	.pipe(
		plumber(function(err) {
			beep();
			console.log('[font_copy] '.magenta + 'There was an issue copying fonts\n' + err.yellow);
			this.emit('end');
		})
	)
	.pipe(gulp.dest('dist/assets/fonts'));
});

// Concatenate & Minify JS task
gulp.task('scripts', function() {
	gulp.src('src/assets/js/*.js')
	.pipe(
		plumber(function(err) {
			beep();
			console.log('[scripts] '.magenta + 'There was an issue uglifying JS\n' + err.yellow);
			this.emit('end');
		})
	)
	.pipe(concat('global.js'))
	.pipe(gulp.dest('dist/assets/js'))
	.pipe(rename('global.min.js'))
	.pipe(uglify())
	.pipe(gulp.dest('dist/assets/js'));
});

// Copies unminified bower components
gulp.task('bower_copy', function() {
	gulp.src([
		'bower_components/html5shiv/dist/html5shiv.js',
		'bower_components/jquery/dist/jquery.js'
	])
	.pipe(
		plumber(function(err) {
			beep();
			console.log('[bower_copy] '.magenta + 'There was an issue copying bower modules\n' + err.yellow);
			this.emit('end');
		})
	)
	.pipe(gulp.dest('dist/assets/js/vendor'));
});

// Copies unminified vendor js
gulp.task('js_copy', function() {
	gulp.src([
		'src/assets/js/vendor/*.js'
	])
	.pipe(
		plumber(function(err) {
			beep();
			console.log('[js_copy] '.magenta + 'There was an issue copying JS\n' + err.yellow);
			this.emit('end');
		})
	)
	.pipe(gulp.dest('dist/assets/js/vendor'));
});

// Copies php files
gulp.task('inc_copy', function() {
	gulp.src([
		'src/inc/**/*.*'
	])
	.pipe(
		plumber(function(err) {
			beep();
			console.log('[inc_copy] '.magenta + 'There was an issue coping includes\n' + err.yellow);
			this.emit('end');
		})
	)
	.pipe(gulp.dest('dist/inc'));
});

// Copies images (don't use in image-heavy projects)
gulp.task('img_copy', function() {
	gulp.src([
		'src/assets/img/**/*.*'
	])
	.pipe(
		plumber(function(err) {
			beep();
			console.log('[img_copy] '.magenta + 'There was an issue copying images\n' + err.yellow);
			this.emit('end');
		})
	)
	.pipe(gulp.dest('dist/assets/img'));
});

// Copy task
gulp.task('copy', function() {
	gulp.src([
		'src/*.php',
		'src/*.htm',
		'src/*.html',
	])
	.pipe(
		plumber(function(err) {
			beep();
			console.log('[copy] '.magenta + 'There was an issue copying the base dir\n' + err.yellow);
			this.emit('end');
		})
	)
	.pipe(gulp.dest('dist'));
});

// Watch task
gulp.task('watch', function() {
	gulp.watch('src/inc/**/*.*', ['inc_copy']);
	gulp.watch('src/assets/js/*.js', ['scripts']);
	gulp.watch('src/assets/js/vendor/*.js', ['js_copy']);
	gulp.watch('src/assets/scss/**/*.scss', ['sass']);
	gulp.watch('src/assets/css/**/*.css', ['css']);
	gulp.watch('src/assets/img/**/*.*', ['img_copy']);
	gulp.watch(
		[
			'src/*.php',
			'src/*.htm',
			'src/*.html',
			'src/.htaccess',
		],
		['copy']
	);

	// reload on file changes
	reload.listen();
	gulp.watch('dist/**').on('change', reload.changed);
});

// Default task
gulp.task('default', ['sass', 'css', 'font_copy', 'scripts', 'copy', 'bower_copy', 'js_copy', 'inc_copy', 'img_copy', 'watch']);